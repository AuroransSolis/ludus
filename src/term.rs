use crate::{class::Class, day_and_time::DayAndTime};
use chrono::{Datelike, NaiveDate, NaiveDateTime};
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Term {
    pub name: String,
    pub path: String,
    pub classes: Vec<Class>,
    pub start: NaiveDate,
    pub end: NaiveDate,
}

impl Term {
    pub fn valid(&self) -> bool {
        if self.classes.is_empty() {
            println!("Term '{}' has no classes!", self.name);
            false
        } else if self.classes.len() == 1 {
            true
        } else {
            for i in 0..self.classes.len() - 1 {
                for j in i + 1..self.classes.len() {
                    if self.classes[i].intersects_with(&self.classes[j]) {
                        println!(
                            "In term '{}', class '{}' intersects with class '{}'.",
                            self.name, self.classes[i].name, self.classes[j].name
                        );
                        return false;
                    }
                }
            }
            true
        }
    }

    pub fn intersects_with(&self, other: &Term) -> bool {
        if (self.start >= other.start && self.start <= other.end)
            || (other.start >= self.start && other.start <= self.end)
        {
            (self.start >= other.start && self.start <= other.end)
                || (other.start >= self.start && other.start <= self.end)
        } else {
            false
        }
    }

    pub fn active_at<'a>(&'a self, now: NaiveDateTime) -> Option<&'a Class> {
        let current_weekday = now.weekday();
        let current_time = now.time();
        let current = DayAndTime {
            day: current_weekday,
            time: current_time,
        };
        self.classes.iter().find(|class| {
            class
                .times
                .iter()
                .any(|class_time| class_time.contains(current))
        })
    }
}
