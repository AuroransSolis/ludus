use std::{
    io::{Error as IoError, ErrorKind, Result as IoResult},
    path::Path,
};

pub fn make_symlink<LF: AsRef<Path>, LT: AsRef<Path>>(from: LF, to: LT) -> IoResult<()> {
    let from = from.as_ref();
    let to = to.as_ref();
    if from == to {
        Err(IoError::new(
            ErrorKind::InvalidInput,
            "Symlink source and destination are the same.",
        ))
    } else {
        if from.exists() {
            if to.exists() {
                if to.metadata()?.file_type().is_symlink() {
                    eprintln!(
                        "Warning: symlink '{}' already exists, overwriting.",
                        to.display()
                    );
                } else {
                    return Err(IoError::new(
                        ErrorKind::AlreadyExists,
                        format!("Symlink destination '{}' already exists.", to.display()),
                    ));
                }
            }
            symlink_wrapper(from, to)
        } else {
            Err(IoError::new(
                ErrorKind::NotFound,
                format!("Specified path '{}' does not exist.", from.display()),
            ))
        }
    }
}

#[cfg(target_family = "unix")]
fn symlink_wrapper<P: AsRef<Path>, Q: AsRef<Path>>(original: P, link: Q) -> IoResult<()> {
    std::os::unix::fs::symlink(original, link)
}

#[cfg(target_family = "windows")]
fn symlink_wrapper<P: AsRef<Path>, Q: AsRef<Path>>(original: P, link: Q) -> IoResult<()> {
    std::os::windows::fs::symlink_dir(from, to)
}
