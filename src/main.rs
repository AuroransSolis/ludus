mod clap;
mod class;
mod config;
mod day_and_time;
mod symlink;
mod term;

use chrono::Local;
use clap::Ludus;
use config::Config;
use std::{
    fs::read_to_string,
    io::{Error as IoError, ErrorKind, Result as IoResult},
    path::{Path, PathBuf},
    str::FromStr,
};
use structopt::StructOpt;

fn main() {
    let usage = Ludus::from_args();
    let config = match get_config(usage.config()) {
        Ok(config) => config,
        Err(e) => {
            eprintln!("Failed to load config. Reason: {:?}", e);
            return;
        }
    };
    match usage {
        Ludus::Auto { .. } => {
            let now = Local::now().naive_local();
            if let Some(term) = config.active_at(now) {
                let term_path = Path::new(&term.path);
                if let Err(err) = symlink::make_symlink(&term_path, &config.term_link) {
                    eprintln!(
                        "Failed to make term symlink from '{}' to '{}'.\nReason: {:?}",
                        term_path.display(),
                        config.term_link,
                        err,
                    );
                    return;
                };
                if let Some(class) = term.active_at(now) {
                    let class_path = if Path::new(&class.path).is_relative() {
                        let mut buf = term_path.to_path_buf();
                        buf.push(&class.path);
                        buf
                    } else {
                        PathBuf::from_str(&class.path).unwrap()
                    };
                    if let Err(err) = symlink::make_symlink(&class_path, &config.class_link) {
                        eprintln!(
                            "Failed to make class symlink from '{}' to '{}'.\nReason: {:?}",
                            class_path.display(),
                            config.class_link,
                            err,
                        );
                    };
                }
            }
        }
        clap::Ludus::Manual {
            config: _,
            set_term_name,
            set_class_name,
            force_set_symlink,
        } => {
            if let Some(term) = config.terms.iter().find(|term| term.name == set_term_name) {
                let term_path = Path::new(&term.path);
                if term_path.exists() && !force_set_symlink {
                    eprintln!("Term path '{}' is occupied.", term.path);
                    return;
                } else {
                    if let Err(err) = symlink::make_symlink(term_path, &config.term_link) {
                        eprintln!(
                            "Failed to make term symlink from '{}' to '{}'.\nReason: {:?}",
                            term_path.display(),
                            config.term_link,
                            err
                        );
                        return;
                    }
                }
                if let Some(class_name) = set_class_name {
                    if let Some(class) = term.classes.iter().find(|class| class.name == class_name)
                    {
                        let class_path = if Path::new(&class.path).is_relative() {
                            let mut class_path = term_path.to_path_buf();
                            class_path.push(&class.path);
                            class_path
                        } else {
                            Path::new(&class.path).to_path_buf()
                        };
                        if class_path.exists() && !force_set_symlink {
                            eprintln!("Class path '{}' is occupied.", class_path.display());
                            return;
                        } else {
                            if let Err(err) = symlink::make_symlink(&class_path, &config.class_link)
                            {
                                eprintln!(
                                    "Failed to make class symlink from '{}' to '{}'.\nReason: {:?}",
                                    class_path.display(),
                                    config.class_link,
                                    err
                                );
                                return;
                            }
                        }
                    } else {
                        eprintln!("Could not find class named '{}'.", class_name);
                    }
                }
            } else {
                eprintln!("Could not find term named '{}'.", set_term_name);
            }
        }
    }
}

fn get_config<P: AsRef<Path>>(p: P) -> IoResult<Config> {
    let config_file_contents = read_to_string(p.as_ref())?;
    let config: Config = toml::from_str(&config_file_contents)?;
    if config.valid() {
        Ok(config)
    } else {
        Err(IoError::new(
            ErrorKind::InvalidData,
            "Config file is invalid.",
        ))
    }
}
