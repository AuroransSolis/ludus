use serde::Deserialize;

use crate::day_and_time::DayAndTime;

#[derive(Copy, Clone, Debug, Deserialize)]
pub struct ClassTime {
    start: DayAndTime,
    end: DayAndTime,
}

impl ClassTime {
    pub fn intersects_with(self, other: Self) -> bool {
        self.start.intersects_with(other.start, other.end)
            || other.start.intersects_with(self.start, self.end)
    }

    pub fn contains(self, dat: DayAndTime) -> bool {
        dat.intersects_with(self.start, self.end)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Class {
    pub name: String,
    pub path: String,
    pub times: Vec<ClassTime>,
}

impl Class {
    pub fn intersects_with(&self, other: &Class) -> bool {
        for &self_time in &self.times {
            for &other_time in &other.times {
                if self_time.intersects_with(other_time) {
                    return true;
                }
            }
        }
        false
    }
}
