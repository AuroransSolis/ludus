use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
#[structopt(
    name = "ludus",
    about = "School term and class symlink manager.",
    author = "Aurorans Solis",
    version = "0.1.0"
)]
pub enum Ludus {
    Auto {
        #[structopt(short = "c", long)]
        /// Path to the config file containing the schedule and other config keys.
        config: String,
    },
    Manual {
        #[structopt(short = "c", long)]
        /// Path to the config file containing the schedule and other config keys.
        config: String,
        #[structopt(long = "term")]
        /// Set the term to the specified term
        set_term_name: String,
        #[structopt(long = "class", requires("set_term_name"))]
        /// Set the class to the specified class
        set_class_name: Option<String>,
        #[structopt(short = "f", long = "force", requires("set_term_name"))]
        /// Forcibly set the term and class symlinks even if it's classtime.
        force_set_symlink: bool,
    },
}

impl Ludus {
    pub fn config<'a>(&'a self) -> &'a str {
        match self {
            Ludus::Auto { config } => config.as_str(),
            Ludus::Manual { config, .. } => config.as_str(),
        }
    }
}
