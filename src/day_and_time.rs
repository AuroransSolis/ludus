use chrono::{NaiveTime, Weekday};
use serde::Deserialize;
use std::cmp::Ordering;

#[derive(Clone, Copy, Debug, Deserialize, Eq, PartialEq)]
pub struct DayAndTime {
    pub day: Weekday,
    pub time: NaiveTime,
}

impl DayAndTime {
    pub fn intersects_with(self, start: Self, end: Self) -> bool {
        let start_from_sun = start.day.num_days_from_sunday();
        let mut end_from_sun = end.day.num_days_from_sunday();
        if end_from_sun < start_from_sun {
            end_from_sun += 7;
        }
        let self_from_sun = self.day.num_days_from_sunday();
        let gt_start = match self_from_sun.cmp(&start_from_sun) {
            Ordering::Equal => self.time >= start.time,
            Ordering::Greater => true,
            Ordering::Less => false,
        };
        let lt_end = match self_from_sun.cmp(&end_from_sun) {
            Ordering::Equal => self.time <= end.time,
            Ordering::Less => true,
            Ordering::Greater => false,
        };
        gt_start && lt_end
    }
}
