use crate::term::Term;
use chrono::NaiveDateTime;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    pub terms: Vec<Term>,
    pub school_dir: String,
    pub term_link: String,
    pub class_link: String,
}

impl Config {
    pub fn valid(&self) -> bool {
        if self.terms.is_empty() {
            println!("No terms defined!");
            false
        } else {
            for term in &self.terms {
                if !term.valid() {
                    return false;
                }
            }
            if self.terms.len() > 1 {
                for i in 0..self.terms.len() - 1 {
                    for j in i + 1..self.terms.len() {
                        if self.terms[i].intersects_with(&self.terms[j]) {
                            println!(
                                "Term '{}' intersects with term '{}'.",
                                self.terms[i].name, self.terms[j].name
                            );
                            return false;
                        }
                    }
                }
            }
            true
        }
    }

    pub fn active_at<'a>(&'a self, now: NaiveDateTime) -> Option<&'a Term> {
        let current_date = now.date();
        self.terms
            .iter()
            .find(|term| term.start <= current_date && current_date <= term.end)
    }
}
